var ws = require("nodejs-websocket");
var User = require("./src/user");
var db = require("./src/db");
var util = require('util');

var work = false;

global.users = {};
global.games = {};

var server = ws.createServer(function(conn){
    new User(conn);
});

server.listen(2100);

setInterval(function(){
    if (!work) {
        work = true;
        Object.keys(games).forEach(function(elem){            
            games[elem].work();
        });
        work = false;
    }

}, 200);
