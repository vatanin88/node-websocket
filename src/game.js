var util = require('util');
var events = require('events');
var db = require('./db');

function Game(user) {
    var that = this;
    this.imageUrl = 'http://v.alt-idea.ru/post-server';
    this.maxUsers = 2;
    this.minUsers = 2;
    this.stepOneTime = 40;
    this.stepTwoTime = 40;
    this.stepThreeTime = 40;

    this.sendAnswers = 0;
    this.sendLikes = 0;

    this.usersInGame = {};
    this.usersInGame[user.model.id] = user;

    user.game = this;

    this.step = 1;
    this.stepStartTime = Date.now();
    this.update = true;
    this.model = {};

    db.query('INSERT INTO game SET ?', {"startUser": user.model.id}, function(err, results) {

        games[results.insertId] = that;
        that.model.id = results.insertId;
        user.saveQuestion();
    });

    events.EventEmitter.call(this);
}

util.inherits(Game,events.EventEmitter);
module.exports = Game;

Game.prototype.work = function() {
    switch(this.step) {
        case 1:
            this.stepOne();
            break;
        case 2:
            this.stepTwo();
            break;
        case 3:
            this.stepThree();
            break;
        case 4:
            this.endGame();
            break;
    }
};

Game.prototype.dataForStepOne = function(user) {
    var result = {}, that = this;
    result.users = [];
    Object.keys(this.usersInGame).forEach(function(elem){
        var obj = {
            "userId" : that.usersInGame[elem].model.id,
            "name" : that.usersInGame[elem].model.name,
            "gender" : that.usersInGame[elem].model.gender,
            "birthday" : that.usersInGame[elem].model.birthday,
            "image1" : (that.usersInGame[elem].model.image1) ? that.imageUrl+ that.usersInGame[elem].model.image1 : '',
        };
        result.users.push(obj);
    });
    return  result;
};

Game.prototype.stepOne = function() {
    var that = this;
    if (this.update) {
        this.update = false;
        Object.keys(this.usersInGame).forEach(function(elem){
            if (that.usersInGame[elem].active)
            that.usersInGame[elem].send("preparation", that.dataForStepOne(that.usersInGame[elem]));
        });
    }

    if (this.stepStartTime + this.stepOneTime*1000 < Date.now()  &&  Object.keys(this.usersInGame).length < this.minUsers) {
        this.closeGame();
        return;
    }

    if (this.stepStartTime + this.stepOneTime*1000 < Date.now()  &&  Object.keys(this.usersInGame).length >= this.minUsers) {
        this.step = 2;
        this.update = true;
        this.stepStartTime = Date.now();
        return;
    }

    if (Object.keys(this.usersInGame).length == this.maxUsers) {
        this.newStep();
        this.step = 2;
        this.update = true;
        this.stepStartTime = Date.now();
    }

};


Game.prototype.dataForStepTwo = function(user) {
    var result = {}, that = this;
    result.users = [];
    result.timer = Math.floor((this.stepStartTime + this.stepTwoTime*1000 - Date.now())/1000);

    Object.keys(this.usersInGame).forEach(function(elem){

        var myAnswer='';

        if (user.model.id == that.usersInGame[elem].model.id) {
            return;
        }

        if (user.ansewrs[that.usersInGame[elem].model.id]) {
            myAnswer = user.ansewrs[that.usersInGame[elem].model.id];
        }

        var obj = {
            "userId" : that.usersInGame[elem].model.id,
            "name" : that.usersInGame[elem].model.name,
            "gender" : that.usersInGame[elem].model.gender,
            "birthday" : that.usersInGame[elem].model.birthday,
            "image1" : (that.usersInGame[elem].model.image1) ? that.imageUrl+ that.usersInGame[elem].model.image1 : '',
            "image2" : (that.usersInGame[elem].model.image2) ? that.imageUrl+ that.usersInGame[elem].model.image2 : '',
            "image3" : (that.usersInGame[elem].model.image3) ? that.imageUrl+ that.usersInGame[elem].model.image3 : '',
            "image4" : (that.usersInGame[elem].model.image4) ? that.imageUrl+ that.usersInGame[elem].model.image4 : '',
            "image5" : (that.usersInGame[elem].model.image5) ? that.imageUrl+ that.usersInGame[elem].model.image5 : '',
            "image6" : (that.usersInGame[elem].model.image6) ? that.imageUrl+ that.usersInGame[elem].model.image6 : '',
            "question" : that.usersInGame[elem].question,
            "myAnswer" : myAnswer
        };
        result.users.push(obj);
    });    

    return  result;
};


Game.prototype.stepTwo= function() {
    var that = this;
    if (this.update) {
        this.update = false;
        this.updateStatus('stepTwo');
        Object.keys(this.usersInGame).forEach(function(elem){
            if (that.usersInGame[elem].active)
            that.usersInGame[elem].send("answerQuestions", that.dataForStepTwo(that.usersInGame[elem]));
        });
    }



    if (this.stepStartTime + this.stepTwoTime*1000 < Date.now() || this.sendAnswers == Object.keys(this.usersInGame).length) {
        this.newStep();
        this.step = 3;
        this.update = true;
        this.stepStartTime = Date.now();
    }

};



Game.prototype.dataForStepThree = function(user) {
    var result = {}, that = this;
    result.users = [];
    result.timer = Math.floor((this.stepStartTime + this.stepThreeTime*1000 - Date.now())/1000);

    Object.keys(this.usersInGame).forEach(function(elem){

        var answer='';
        var like = false;

        if (user.model.id == that.usersInGame[elem].model.id) {
            return;
        }

        if (that.usersInGame[elem].ansewrs[user.model.id]) {
            answer = that.usersInGame[elem].ansewrs[user.model.id];
        }

        if (user.like == that.usersInGame[elem].model.id) {
            like = true;
        }

        var obj = {
            "userId" : that.usersInGame[elem].model.id,
            "name" : that.usersInGame[elem].model.name,
            "gender" : that.usersInGame[elem].model.gender,
            "birthday" : that.usersInGame[elem].model.birthday,
            "image1" : (that.usersInGame[elem].model.image1) ? that.imageUrl+ that.usersInGame[elem].model.image1 : '',
            "image2" : (that.usersInGame[elem].model.image2) ? that.imageUrl+ that.usersInGame[elem].model.image2 : '',
            "image3" : (that.usersInGame[elem].model.image3) ? that.imageUrl+ that.usersInGame[elem].model.image3 : '',
            "image4" : (that.usersInGame[elem].model.image4) ? that.imageUrl+ that.usersInGame[elem].model.image4 : '',
            "image5" : (that.usersInGame[elem].model.image5) ? that.imageUrl+ that.usersInGame[elem].model.image5 : '',
            "image6" : (that.usersInGame[elem].model.image6) ? that.imageUrl+ that.usersInGame[elem].model.image6 : '',
            "question" : that.usersInGame[elem].question,
            "answer" : answer,
            "myLikeToAnswer" : like
        };
        result.users.push(obj);
    });    

    return  result;
};


Game.prototype.stepThree= function() {
    var that = this;
    if (this.update) {
        this.update = false;
        this.updateStatus('stepThree');
        Object.keys(this.usersInGame).forEach(function(elem){
            if (that.usersInGame[elem].active)
            that.usersInGame[elem].send("choiceFavorite", that.dataForStepThree(that.usersInGame[elem]));
        });
    }


    if (this.stepStartTime + this.stepThreeTime*1000 < Date.now() || this.sendLikes == Object.keys(this.usersInGame).length) {
        this.newStep();
        this.step = 4;
        this.update = true;
        this.stepStartTime = Date.now();
    }


};



Game.prototype.addToGame = function(user) {
    if (this.step == 1 && Object.keys(this.usersInGame).length < this.maxUsers  ) {
        this.update = true;
        this.usersInGame[user.model.id] = user;
        user.game = this;
        user.saveQuestion();
        return true;
    }
    return false;
};

Game.prototype.closeGame = function() {
    var that = this;
    this.updateStatus('close');
    Object.keys(this.usersInGame).forEach(function(elem){
        that.usersInGame[elem].send("notGame");
        delete users[elem];
        that.usersInGame[elem].conn.close();

    });


    delete games[this.model.id];

};


Game.prototype.endGame = function() {
    var that = this;
    this.updateStatus('end');
    Object.keys(this.usersInGame).forEach(function(elem){        
        that.usersInGame[elem].saveAnswersAndLikes();
    });

    Object.keys(this.usersInGame).forEach(function(elem){
        that.usersInGame[elem].send("endGame", {"gameId": that.model.id});        
        delete users[elem];
        that.usersInGame[elem].conn.close();
    });

    delete games[this.model.id];

};

Game.prototype.deleteUser = function(user) {    
    delete this.usersInGame[user.model.id];
    delete users[user.model.id];
    user.conn.close();
};

Game.prototype.updateStatus = function(status) {
    db.query('UPDATE game SET ? WHERE ?', [{"status": status}, {"id":this.model.id}], function(err, results) {

    });
};

Game.prototype.counter = function(counter) {
    if (counter == 'answer') {
        this.sendAnswers++;
    }

    if (counter == 'like') {
        this.sendLikes++;
    }
};

Game.prototype.newStep = function() {
    var that = this;
    Object.keys(this.usersInGame).forEach(function(elem){        
        that.usersInGame[elem].getMessage = 0;
    });
};


Game.prototype.retryConnect = function(user) {

    switch(this.step) {
        case 1:
            user.send("preparation", this.dataForStepOne(user));
            break;
        case 2:
            user.send("answerQuestions", this.dataForStepTwo(user));
            break;
        case 3:
            user.send("choiceFavorite", this.dataForStepThree(user));
            break;
    }

};