var util = require('util');
var events = require('events');
var db = require('./db');
var Game = require('./game');

function User(conn) {
    var that = this;
    this.conn = conn;
    this.question = '';
    this.ansewrs = {};
    this.active = 1;

    this.getMessage = 0;

    this.questionId = 0;
    this.like = 0;

    this.conn.on('text', function(str) {that.onMessage.call(that, str)});
    events.EventEmitter.call(this);
}

util.inherits(User,events.EventEmitter);
module.exports = User;

User.prototype.send = function(name, params) {
    if (!params) {
        this.conn.sendText(name+"\n");
    } else {
        this.conn.sendText(name+"::"+ JSON.stringify(params) +"\n");
    }


    this.emit('send', name, params);
};

User.prototype.setSession = function(params) {
    var that = this;
    if(users[params.Session]) {
        users[params.Session].conn.close();
        this.game = users[params.Session].game;
        this.getMessage =  users[params.Session].getMessage;
        this.model = users[params.Session].model;
        this.question = users[params.Session].question;
        this.game.usersInGame[this.model.id] = this;

        users[params.Session] = this;

        this.conn.on('close', function() {
            that.active = 0;
        });

        this.game.retryConnect(this);

        return;
    }

    
    db.query('SELECT * FROM user WHERE id = ?', params.Session, function(err, results) {
        if (!results.length) {
            //TODO Нет пользователя
            that.conn.close();
            return;
        }
        users[params.Session] = that;
        that.question = params.question;

        that.model = results[0];

        that.conn.on('close', function() {
            that.active = 0;
        });

        that.emit('setSession', params);
        that.findGame();
        console.log("User connected");
    });
};


User.prototype.answer = function(params) {
    this.ansewrs[params.userId] = params.myAnswer;
    this.game.counter('answer');
};

User.prototype.likeUserAnswer = function(params) {
    this.like = params.userId;
    this.game.counter('like');
};

User.prototype.cancelSession = function() {
    this.game.deleteUser(this);
};

User.prototype.finishStep = function(params) {
    if (this.game.step == 2) {
        this.game.sendAnswers++;
    } else {
        this.game.sendLikes++;
    }
};


User.prototype.onMessage = function(str) {

    if (this.getMessage) {
        return;
    }

    this.getMessage = 1;

    var command = str.split('::');    

    if (command[0] != 'cancelSession') {
        command[1] = JSON.parse(command[1]);
    }

    switch(command[0]) {
        case 'setsession':
            this.setSession(command[1]);
            break;
        case 'answer':
            this.answer(command[1]);
            break;
        case 'likeUserAnswer':
            this.likeUserAnswer(command[1]);
            break;
        case 'cancelSession':
            this.cancelSession();
            break;
        case 'finishStep':
            this.finishStep(command[1]);
            break;
    }
};


User.prototype.findGame = function() {
    if (!Object.keys(games).length) {
        new Game(this);
    } else {
        //TODO Поиск игр
        var that = this, find = false;
        Object.keys(games).forEach(function(elem){
            if (games[elem].addToGame(that)) {
                find = true;
                return;
            }
        });

        if (!find) {
            new Game(this);
        }
    }
    this.emit('findGame');
};

User.prototype.saveQuestion = function() {
    var that = this;
    db.query('INSERT INTO question SET ?', {"question": this.question, "game":this.game.model.id, "user":this.model.id}, function(err, results) {
        that.questionId = results.insertId;
    });
};

User.prototype.saveAnswersAndLikes = function() {
    var that = this;
    if (this.like) {
        db.query('INSERT INTO `like` SET ?', {"like_user": this.like, "game":this.game.model.id, "user":this.model.id, "like": 1}, function(err, results) {

        });
    }

    Object.keys(this.ansewrs).forEach(function(elem){

        if (!users[elem]) {
            return;
        }
        
        db.query('INSERT INTO answer SET ?', {"question": users[elem].questionId, "game":that.game.model.id, "user":that.model.id, "answer": that.ansewrs[elem]}, function(err, results) {
            
        });
    });

};

